# Braid

[![pipeline status](https://gitlab.com/cordite/braid/badges/master/pipeline.svg)](https://gitlab.com/cordite/braid/commits/master)

![logo](braid-docs/src/site/sphinx/images/logo-small.png) 

 
## Introduction

A Kotlin / Java communication library, implementing REST, JSON-RPC and a streaming-over-sockets extension to JSON-RPC.

The library can be used in any Kotlin / Java project using [braid-standalone-server](braid-standalone-server/README.md).

It can also be incorporated in [Corda](https://corda.net) Cordapps using [braid-corda](braid-corda/README.md). 

> To get started quickly with braid and Corda see the [Kotlin Cordapp temlate project with braid](https://gitlab.com/cordite/cordapp-template-kotlin-with-braid).

These both use the [core](braid-core/README.md) module. 

### Consuming Services 

The REST endpoint can be consumed using the `swagger.json` emitted by the server.
The socket JSON-RPC endpoint can be consumed using the following languages and runtimes:

* [javascript](braid-client-js) in a browser or NodeJS
* [any JVM language](braid-client)

### Protocol

If you want to implement your own client, or are just interested in how Braid works, the 
protocol is defined **[here](./braid-core/README.md)**.

## Examples

* [`example-cordapp`](examples/example-cordapp) - an example Corda cordapp, with customised authentication, streaming
* [`example-server`](examples/example-server) - a simple standalone server 
* [another cordapp](https://github.com/joeldudleyr3/pigtail) - another example by [Joel Dudley](https://twitter.com/joeldudley6)


## Building locally

You will need:

* Maven 3.6.x
* Node JS 12.18.3 or greater.

The build for all modules (Kotlin, Javascript, Cordapps etc) is orchestrated with maven.

To compile, test and install the dependencies:

```bash
mvn clean install
```

The project can be loaded into any IDE that can read Maven POMs.

## Publishing / Deploying Artifacts

1. Checkout master
2. Make sure you pull the latest master
3. Use maven versions plugin to bump the version to the next snapshot

```
mvn versions:set -DnewVersion=3.0.3-SNAPSHOT
```

4. Git add and commit 
5. Don’t push yet (to speed up the process)
6. Git create a new branch off master with the correct release version e.g. v3.0.2

```
git checkout -b v3.0.2
```

7. Push this release branch (ie not the master branch)
8. In gitlab CI there is a manual job for the branch that you will kick off
9. Then checkout master and push it
10. Log into [https://oss.sonatype.org/](https://oss.sonatype.org/)
11. Go to the `Staging Repositories` tab, and search for `cordite`, locating the current staged release.
12. Close the release and release it.

Obviously the above should be automated with a script - ideally integrated worth maven

