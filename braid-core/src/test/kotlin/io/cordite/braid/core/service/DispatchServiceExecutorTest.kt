/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.service

import io.cordite.braid.core.jsonrpc.JsonRPCRequest
import io.vertx.core.Future
import org.junit.Assert.assertEquals
import org.junit.Test

class DispatchServiceExecutorTest {
  private val apple = "apple"
  private val orange = "orange"
  private val serviceApple = TestService(apple)
  private val serviceOrange = TestService(orange)
  private val appleExecutor = ConcreteServiceExecutor(serviceApple)
  private val orangeExecutor = ConcreteServiceExecutor(serviceOrange)
  private val dispatchExecutor = DispatchServiceExecutor(
    mapOf(
      apple to appleExecutor,
      orange to orangeExecutor
    )
  )

  @Test
  fun `that dispatcher invokes the correct service`() {
    val appleRequest = JsonRPCRequest(id = 0, method = "$apple.name", params = null)
    val appleResult = dispatchExecutor.invoke(appleRequest).single().toBlocking().first().toString()
    assertEquals(apple, appleResult)

    val orangeRequest = JsonRPCRequest(id = 0, method = "$orange.name", params = null)
    val orangeResult = dispatchExecutor.invoke(orangeRequest).single().toBlocking().first().toString()
    assertEquals(orange, orangeResult)
  }

  @Test
  fun `that dispatcher fails with unknown service`() {
    val request = JsonRPCRequest(id = 0, method = "foo.name", params = null)
    try {
      dispatchExecutor.invoke(request).single().toBlocking().first().toString()
      error("should have failed")
    } catch (ex: Throwable) {
      val msg = ex.message ?: error("failed to receive exception with message")
      if (!msg.contains("unknown service") || !msg.contains("foo")) {
        error("failed to get exception message with both 'unknown service' and 'foo'")
      }
    }
  }

  @Test
  fun `that dispatcher invokes the correct service async function`() {
    val appleRequest = JsonRPCRequest(id = 0, method = "$apple.asyncOp", params = listOf(apple))
    val result = dispatchExecutor.invoke(appleRequest).single().toBlocking().first().toString()
    assertEquals(apple, result)
  }
}

class TestService(private val name: String) {
  fun name() = name
  fun asyncOp(param: String): Future<String> {
    return Future.succeededFuture(param)
  }
}