/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.service

import io.cordite.braid.core.jsonrpc.JsonRPCRequest
import io.cordite.braid.core.jsonschema.toDescriptor
import io.cordite.braid.core.logging.loggerFor
import rx.Observable
import rx.Subscriber
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Modifier
import kotlin.reflect.KFunction
import kotlin.reflect.KVisibility
import kotlin.reflect.full.functions
import kotlin.reflect.jvm.javaType

class ConcreteServiceExecutor(
  private val service: Any,
  private val resultAdapters: List<JsonRpcResultAdapter<*>> = defaultAdapters
) : ServiceExecutor {

  companion object {

    private val log = loggerFor<ConcreteServiceExecutor>()

    @Suppress("UNCHECKED_CAST")
    val defaultAdapters: List<JsonRpcResultAdapter<*>> = listOf(
      VertxFutureResultAdapter,
      ObservableResultAdapter
    )
  }

  @Suppress("UNCHECKED_CAST")
  fun <T> withAdapter(adapter: JsonRpcResultAdapter<T>): ConcreteServiceExecutor {
    return ConcreteServiceExecutor(service, resultAdapters + adapter)
  }

  override fun invoke(request: JsonRPCRequest): Observable<Any> {
    return Observable.unsafeCreate { subscriber ->
      request.withMDC {
        try {
          log.trace("binding to method for {}", request)
          candidateMethods(request)
            .asSequence() // lazy sequence
            .convertParametersAndFilter(request)
            .map { (method, params) ->
              if (log.isTraceEnabled) {
                log.trace(
                  "invoking ${method.asSimpleString()} with ${params.joinToString(
                    ","
                  ) { it.toString() }}"
                )
              }
              method.call(service, *params).also {
                if (log.isTraceEnabled) {
                  log.trace(
                    "successfully invoked ${method.asSimpleString()} with ${params.joinToString(
                      ","
                    ) { it.toString() }}"
                  )
                }
              }
            }
            .firstOrNull()
            ?.also { result -> handleResult(result, request, subscriber) }
            ?: throwMethodDoesNotExist(request)
        } catch (err: InvocationTargetException) {
          log.error("failed to invoke target for $request", err)
          subscriber.onError(err.targetException)
        } catch (err: Throwable) {
          log.error("failed to invoke $request", err)
          subscriber.onError(err)
        }
      }
    }
  }

  private fun KFunction<*>.asSimpleString(): String {
    val params = this.parameters.drop(1)
      .joinToString(",") { "${it.name}: ${it.type.javaType.typeName}" }
    return "$name($params)"
  }

  private fun candidateMethods(request: JsonRPCRequest): List<KFunction<*>> {
    return service::class.functions
      .filter(request::matchesName)
      .filter(this::isPublic)
      .filter { it.parameters.size == request.paramCount() + 1 }
      .map { it to request.computeScore(it) }
      .filter { (_, score) -> score > 0 }
      .sortedByDescending { (_, score) -> score }
      .also {
        if (log.isTraceEnabled) {
          log.trace("scores for candidate methods for {}:", request)
          it.forEach {
            println("${it.second}: ${it.first.asSimpleString()}")
          }
        }
      }
      .map { (fn, _) -> fn }
  }

  private fun throwMethodDoesNotExist(request: JsonRPCRequest) {
    throw MethodDoesNotExist("failed to find a method that matches ${request.method}(${request.paramsAsString()})")
  }

  private fun Sequence<KFunction<*>>.convertParametersAndFilter(request: JsonRPCRequest): Sequence<Pair<KFunction<*>, Array<Any?>>> {
    // attempt to convert the parameters
    return map { method ->
      method to try {
        request.mapParams(method)
      } catch (err: Throwable) {
        null
      }
    }
      // filter out parameters that didn't match
      .filter { (_, params) -> params != null }
      .map { (method, params) -> method to params!! }
  }

  @Suppress("UNCHECKED_CAST")
  private fun handleResult(
    result: Any?,
    request: JsonRPCRequest,
    subscriber: Subscriber<Any>
  ) {
    log.trace("handling result {}", request.id, result)
    val adapter = (resultAdapters.firstOrNull() {
      it.handledClass.isInstance(result)
    } ?: DefaultResultAdapter) as JsonRpcResultAdapter<Any>
    adapter.handle(request, result, subscriber)
  }

  private fun isPublic(method: KFunction<*>) = method.visibility == KVisibility.PUBLIC

  override fun getMethodDescriptors(): List<MethodDescriptor> {
    return service.javaClass.declaredMethods
      .filter { Modifier.isPublic(it.modifiers) }
      .filter { !it.name.contains("$") }
      .map { it.toDescriptor() }
  }
}