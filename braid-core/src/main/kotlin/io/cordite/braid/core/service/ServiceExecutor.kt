/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.service

import io.cordite.braid.core.jsonrpc.JsonRPCRequest
import io.vertx.core.json.JsonObject
import rx.Observable

class MethodDoesNotExist(val methodName: String) : Exception()

interface ServiceExecutor {
  fun invoke(request: JsonRPCRequest): Observable<Any>
  fun getMethodDescriptors(): List<MethodDescriptor>
}

/**
 * this isn't a Kotlin data class, because getting Jackson kotlin databind to transform the [parameters] and [returnType]
 * to json objects did not work
 */
class MethodDescriptor(
  val name: String,
  val description: String,
  val parameters: JsonObject,
  val returnType: JsonObject
) {

  fun withName(name: String): MethodDescriptor = MethodDescriptor(name, description, parameters, returnType)
  fun withReturnType(returnType: JsonObject) = MethodDescriptor(name, description, parameters, returnType)
  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (javaClass != other?.javaClass) return false

    other as MethodDescriptor

    if (name != other.name) return false
    if (description != other.description) return false
    if (parameters != other.parameters) return false
    if (returnType != other.returnType) return false

    return true
  }

  override fun hashCode(): Int {
    var result = name.hashCode()
    result = 31 * result + description.hashCode()
    result = 31 * result + parameters.hashCode()
    result = 31 * result + returnType.hashCode()
    return result
  }

  override fun toString(): String {
    return "MethodDescriptor(name='$name', description='$description', parameters=$parameters, returnType=$returnType)"
  }

}
