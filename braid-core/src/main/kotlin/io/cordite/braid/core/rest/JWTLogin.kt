/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
@file:Suppress("DEPRECATION")

package io.cordite.braid.core.rest

import io.cordite.braid.core.logging.loggerFor
import io.swagger.annotations.ApiModelProperty
import io.swagger.annotations.ApiOperation
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.json.Json
import io.vertx.core.json.JsonObject
import io.vertx.ext.auth.AuthProvider
import io.vertx.ext.auth.PubSecKeyOptions
import io.vertx.ext.auth.User
import io.vertx.ext.auth.jwt.JWTAuth
import io.vertx.ext.auth.jwt.JWTAuthOptions
import io.vertx.ext.jwt.JWTOptions

/**
 * Authentication service for the REST service
 */
class JWTLogin(private val authProvider: AuthProvider, private val jwtSecret: String) {

  companion object {
    val log = loggerFor<JWTLogin>()
  }

  private lateinit var jwtAuth: JWTAuth

  @ApiOperation(value = "login into this API and use the result as a bearer token")
  fun login(request: LoginRequest): Future<String> {
    val authFuture = Future.future<User>()
    authProvider.authenticate(JsonObject(Json.encode(request)), authFuture)
    return authFuture.map { jwtAuth.generateToken(JsonObject().put("user", request.username), JWTOptions().setExpiresInMinutes(24 * 60)) }
  }

  fun createJWTAuthProvider(vertx: Vertx) : JWTAuth {
    if (!::jwtAuth.isInitialized) {
      val jwtAuthOptions = JWTAuthOptions()
        .addPubSecKey(
          PubSecKeyOptions()
            .setAlgorithm("HS256")
            .setPublicKey(jwtSecret)
            .setSymmetric(true)
        )
      jwtAuth = JWTAuth.create(vertx, jwtAuthOptions)
    }
    return jwtAuth
  }
}

data class LoginRequest(
  @ApiModelProperty(value = "user name", example = "sa") val username: String,
  @ApiModelProperty(value = "password", example = "admin") val password: String
)
