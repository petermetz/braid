/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.jsonschema

import com.fasterxml.jackson.databind.JavaType
import com.fasterxml.jackson.databind.type.TypeFactory
import com.fasterxml.jackson.module.jsonSchema.JsonSchema
import com.fasterxml.jackson.module.jsonSchema.factories.SchemaFactoryWrapper
import com.fasterxml.jackson.module.jsonSchema.types.ObjectSchema
import io.cordite.braid.core.annotation.MethodDescription
import io.cordite.braid.core.annotation.ServiceDescription
import io.cordite.braid.core.reflection.underlyingGenericType
import io.cordite.braid.core.service.MethodDescriptor
import io.vertx.core.json.JsonObject
import io.vertx.core.json.jackson.DatabindCodec
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import rx.Observable
import java.lang.reflect.Constructor
import java.lang.reflect.Method

fun Method.toDescriptor(): MethodDescriptor {
  val serviceAnnotation = getAnnotation<MethodDescription>(MethodDescription::class.java)
  val name = this.name
  val params = json { obj(parameters.map { it.name to it.type.toJavascriptType() }.toMap()) }

  val returnDescription = when {
    serviceAnnotation != null && serviceAnnotation.returnType != Any::class ->
      serviceAnnotation.returnType.javaObjectType.toJavascriptType()
    else -> {
      TypeFactory.defaultInstance().constructType(genericReturnType.underlyingGenericType()).toJavascriptType()
    }
  }

  if (returnType == Observable::class.java) {
    returnDescription.put("x-json-rpc-streaming", true)
  }
  val description = serviceAnnotation?.description ?: ""
  return MethodDescriptor(name, description, params, returnDescription)
}

fun <T : Any> Constructor<T>.toDescriptor(): MethodDescriptor {
  val description = this.declaringClass.getAnnotation(ServiceDescription::class.java)?.description ?: ""
  val name = this.name
  val params = json { obj(parameters.map { it.name to it.type.toJavascriptType() }.toMap()) }
  val returnDescription = json { obj("type" to this@toDescriptor.declaringClass.simpleName) }
  return MethodDescriptor(name = name, description = description, parameters = params, returnType = returnDescription)
}

fun Class<*>.toJavascriptType() = describeClass(this)
fun JavaType.toJavascriptType() = describeJavaType(this)

fun describeClassSimple(clazz: Class<*>): Any {
  return if (clazz.isPrimitive || clazz == String::class.java) {
    describeClass(clazz)
  } else if (clazz.isArray) {
    "array"
  } else {
    clazz.simpleName
  }
}

fun describeClass(clazz: Class<*>): JsonObject {
  val mapper = DatabindCodec.mapper()
  val visitor = SchemaFactoryWrapper()
  mapper.acceptJsonFormatVisitor(clazz, visitor)
  return describe(visitor.finalSchema())
}

fun describeJavaType(type: JavaType): JsonObject {
  val mapper = DatabindCodec.mapper()
  val visitor = SchemaFactoryWrapper()
  mapper.acceptJsonFormatVisitor(type, visitor)
  return describe(visitor.finalSchema())
}

private fun describe(value: JsonSchema) = describeAsObject(value)

private fun describeAsObject(value: JsonSchema): JsonObject {
  return if (value is ObjectSchema) {
    describeProperties(value)
  } else {
    @Suppress("UNCHECKED_CAST") val result =
      DatabindCodec.mapper().convertValue(value, Map::class.java) as Map<String, Any>
    JsonObject(result)
  }
}

private fun describeProperties(value: ObjectSchema): JsonObject {
  return json {
    obj {
      val jo = this
      value.properties.forEach {
        jo.put(it.key, describeAsObject(it.value))
      }
    }
  }
}