/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.service

import io.cordite.braid.core.jsonrpc.JsonRPCRequest
import io.cordite.braid.core.jsonrpc.createJsonException
import io.cordite.braid.core.logging.loggerFor
import io.vertx.core.AsyncResult
import io.vertx.core.Future
import rx.Observable
import rx.Subscriber

/**
 * Interface for adapters of a result type [T] such that it is bound to a [Subscriber<T>].
 */
abstract class JsonRpcResultAdapter<T> {

  companion object {

    private val log = loggerFor<JsonRpcResultAdapter<*>>()
  }

  abstract val handledClass: Class<T>
  abstract fun handle(request: JsonRPCRequest, result: T?, subscriber: Subscriber<Any>)

  fun send(result: Any?, subscriber: Subscriber<Any>) {
    log.trace("sending result and completing {}", result)
    subscriber.onNext(result)
    subscriber.onCompleted()
  }

  fun send(err: Throwable, subscriber: Subscriber<Any>) {
    log.trace("sending error {}", err)
    subscriber.onError(err)
  }
}

object DefaultResultAdapter : JsonRpcResultAdapter<Any>() {

  override val handledClass: Class<Any> = Any::class.java
  override fun handle(request: JsonRPCRequest, result: Any?, subscriber: Subscriber<Any>) {
    send(result, subscriber)
  }
}

object VertxFutureResultAdapter : JsonRpcResultAdapter<Future<*>>() {

  private val log = loggerFor<VertxFutureResultAdapter>()
  override val handledClass: Class<Future<*>> = Future::class.java

  @Suppress("UNCHECKED_CAST")
  override fun handle(request: JsonRPCRequest, result: Future<*>?, subscriber: Subscriber<Any>) {
    check(result != null)
    request.withMDC {
      log.trace("{} - handling future result", request.id)
      val future = result as Future<Any>
      future.setHandler {
        handleAsyncResult(it, request, subscriber)
      }
    }
  }

  private fun handleAsyncResult(
    response: AsyncResult<*>,
    request: JsonRPCRequest,
    subscriber: Subscriber<Any>
  ) {
    request.withMDC {
      log.trace("{} - handling async result of invocation", request.id)
      when (response.succeeded()) {
        true -> send(response.result(), subscriber)
        else -> send(response.cause().createJsonException(request), subscriber)
      }
    }
  }
}

object ObservableResultAdapter : JsonRpcResultAdapter<Observable<*>>() {

  private val log = loggerFor<ObservableResultAdapter>()

  override val handledClass: Class<Observable<*>> = Observable::class.java

  override fun handle(request: JsonRPCRequest, result: Observable<*>?, subscriber: Subscriber<Any>) {
    check(result != null)
    log.trace("{} - handling observable result", request.id)
    result!!.onErrorResumeNext { err -> Observable.error(err.createJsonException(request)) }
      .let {
        // insert logger if trace is enabled
        request.withMDC {
          if (log.isTraceEnabled) {
            it
              .doOnNext {
                log.trace("sending item {}", it)
              }
              .doOnError {
                log.trace("sending error {}", it)
              }
              .doOnCompleted {
                log.trace("completing stream")
              }
          } else {
            it
          }
        }
      }
      .subscribe(subscriber)
  }

}