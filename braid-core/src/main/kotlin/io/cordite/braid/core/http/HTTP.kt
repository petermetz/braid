/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.http

import io.vertx.core.Future
import io.vertx.core.Promise.promise
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpClient
import io.vertx.core.http.HttpClientRequest
import io.vertx.core.http.HttpClientResponse
import io.vertx.core.http.HttpHeaders
import io.vertx.core.json.Json
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import java.net.URLEncoder
import javax.ws.rs.core.MediaType

fun <T : Any> RoutingContext.write(data: T) {
  when (data) {
    is String -> write(data)
    is Buffer -> write(data)
    else -> {
      val payload = Json.encode(data)
      response()
        .setStatusCode(200)
        .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
        .putHeader(HttpHeaders.CONTENT_LENGTH, payload.length.toString())
        .write(payload)
        .end()
    }
  }
}

fun RoutingContext.write(err: Throwable) {
  response()
    .setStatusCode(500)
    .setStatusMessage(err.message)
    .end()
}

fun RoutingContext.write(str: String) {
  response()
    .setStatusCode(200)
    .putHeader(HttpHeaders.CONTENT_TYPE, "text/plain")
    .putHeader(HttpHeaders.CONTENT_LENGTH, str.length.toString())
    .write(str)
    .end()
}

val HttpClientResponse.failed: Boolean
  get() = (this.statusCode() / 100) != 2

fun HttpClientResponse.body(): Future<Buffer> {
  val result = promise<Buffer>()
  this.bodyHandler { buffer ->
    result.complete(buffer)
  }
  return result.future()
}

fun HttpClientResponse.bodyAsString(): Future<String> {
  return body().map { it.toString() }
}

inline fun <reified T : Any> HttpClientResponse.bodyAsJsonObject(): Future<T> {
  return body().map { Json.decodeValue(it, T::class.java)}
}

inline fun <reified T : Any> HttpClientResponse.bodyAsJsonSet(): Future<Set<T>> {
  return bodyAsJsonList<T>()
    .map { it.toSet() }
}



inline fun <reified T : Any> HttpClientResponse.bodyAsJsonList(): Future<List<T>> {
  val bodyFuture = body()

  return if (T::class == String::class) {
    @Suppress("UNCHECKED_CAST")
    bodyFuture.map { JsonArray(it).map { item -> item.toString() } } as Future<List<T>>
  } else {
    bodyFuture
      .map {
        val decoded = JsonArray(it).map { item ->
          val reEncoded = (item as JsonObject).encode()
          @Suppress("UNCHECKED_CAST")
          Json.decodeValue(reEncoded, T::class.java)
        }
        decoded
      }
  }
}

fun HttpClient.futureGet(
  path: String,
  headers: Map<String, Any> = emptyMap(),
  queryParams: Map<String, String> = emptyMap()
): Future<HttpClientResponse> {
  val result = promise<HttpClientResponse>()
  @Suppress("DEPRECATION")
  get(path.appendQueryParams(queryParams))
    .appendHeaders(headers)
    .exceptionHandler { result.fail(it.cause) }
    .handler { result.complete(it) }
    .end()
  return result.future()
}

fun HttpClient.futurePut(
  path: String,
  headers: Map<String, Any> = emptyMap(),
  queryParams: Map<String, String> = emptyMap()
) = futurePut<Unit>(path, headers, queryParams, null)

fun <T> HttpClient.futurePut(
  path: String,
  headers: Map<String, Any> = emptyMap(),
  queryParams: Map<String, String> = emptyMap(),
  body: T?
): Future<HttpClientResponse> {
  val result = promise<HttpClientResponse>()
  @Suppress("DEPRECATION")
  put(path.appendQueryParams(queryParams))
    .appendHeaders(headers)
    .exceptionHandler { result.fail(it.cause) }
    .handler { result.complete(it) }
    .apply {
      writeBody(body)
    }
  return result.future()
}

fun HttpClientResponse.verifyError(code: Int): HttpClientResponse {
  this.request().path()
  val statusClass = this.statusCode() / 100
  if (statusClass == 2) {
    val simpleMessage =
      "${request().method()} ${request().path()} response did not fail with status code ${statusCode()}"
    if (statusMessage() == null || statusMessage().isBlank()) {
      error(simpleMessage)
    } else {
      error("$simpleMessage: ${statusMessage()}")
    }
  } else {
    check(this.statusCode() == code) { "response failed with status code $statusClass rather than $code" }
  }
  return this
}

fun HttpClientResponse.verifyNoError(): HttpClientResponse {
  this.request().path()
  val statusClass = this.statusCode() / 100
  if (statusClass != 2) {
    val simpleMessage = "${request().method()} ${request().path()} response failed with status code ${statusCode()}"
    if (statusMessage() == null || statusMessage().isBlank()) {
      error(simpleMessage)
    } else {
      error("$simpleMessage: ${statusMessage()}")
    }
  }
  return this
}

fun  HttpClientRequest.writeBody(body: Buffer) {
  putHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_OCTET_STREAM)
  putHeader(HttpHeaders.CONTENT_LENGTH, body.length().toString())
  end(body)
}

fun HttpClientRequest.writeBody(body: String) {
  putHeader(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN)
  putHeader(HttpHeaders.CONTENT_LENGTH, body.length.toString())
  end(body)
}

fun <T> HttpClientRequest.writeBody(body: T?) {
  when(body){
    null -> end()
    is String -> writeBody(body)
    is Buffer -> writeBody(body)
    else -> {
      val bodyJson = Json.encode(body)
      putHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
      putHeader(HttpHeaders.CONTENT_LENGTH, bodyJson.length.toString())
      end(bodyJson)
    }
  }
}

fun HttpClient.futurePost(
  path: String,
  headers: Map<String, Any> = emptyMap(),
  queryParams: Map<String, String> = emptyMap()
) = futurePost<Unit>(path, headers, queryParams, null)

fun <T> HttpClient.futurePost(
  path: String,
  headers: Map<String, Any> = emptyMap(),
  queryParams: Map<String, String> = emptyMap(),
  body: T? = null
): Future<HttpClientResponse> {
  val result = promise<HttpClientResponse>()
  @Suppress("DEPRECATION")
  post(path.appendQueryParams(queryParams))
    .appendHeaders(headers)
    .exceptionHandler { result.fail(it.cause) }
    .handler { result.complete(it) }
    .apply {
      writeBody(body)
    }
  return result.future()
}

@Suppress("UNCHECKED_CAST")
fun HttpClientRequest.appendHeaders(headers: Map<String, Any>): HttpClientRequest {
  headers.forEach {(key, value) ->
    when (value) {
      is Iterable<*> -> {
        val first = value.firstOrNull()
        when(first) {
          null -> {}
          is CharSequence -> putHeader(key, value as Iterable<CharSequence>)
          is String -> putHeader(key, value as Iterable<String>)
          else -> putHeader(key, value.map { it.toString() })
        }
      }
      is String -> putHeader(key, value)
      else -> putHeader(key, value.toString())
    }
  }
  return this
}

fun String.appendQueryParams(queryParams: Map<String, String>): String {
  if (queryParams.isEmpty()) return this
  val queryString = queryParams.map { (key, value) ->
    "$key=${URLEncoder.encode(value, "UTF-8")}"
  }.joinToString("&")
  return when {
    endsWith('?') -> "$this$queryString"
    else -> "$this?$queryString"
  }
}