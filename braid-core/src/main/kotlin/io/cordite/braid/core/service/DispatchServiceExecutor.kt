/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.service

import io.cordite.braid.core.jsonrpc.JsonRPCRequest
import rx.Observable

/**
 * Presents multiple [ServiceExecutor]s as a unified [ServiceExecutor]
 * Each executor is named and can be invoked with [JsonRPCRequest]
 * with [JsonRPCRequest#method] set to `<service-name>.<method>`
 */
class DispatchServiceExecutor(private val namedServiceExecutors: Map<String, ServiceExecutor>) : ServiceExecutor {

  override fun invoke(request: JsonRPCRequest): Observable<Any> {
    val parts = request.method.split(".")
    if (parts.size != 2) {
      error("expected name of the form <service>.<method> but instead got ${request.method}")
    }
    val serviceName = parts[0]
    val methodName = parts[1]
    return namedServiceExecutors[serviceName]?.invoke(request.copy(method = methodName))
      ?: error("unknown service $serviceName")
  }

  override fun getMethodDescriptors(): List<MethodDescriptor> {
    return namedServiceExecutors.flatMap { (serviceName, executor) ->
      executor.getMethodDescriptors().map { it.withName("$serviceName.${it.name}") }
    }
  }
}
