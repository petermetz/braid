/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.json

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.deser.std.NumberDeserializers
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.cordite.braid.core.jsonrpc.JsonRPCRequest
import io.cordite.braid.core.jsonrpc.JsonRPCResultResponse
import io.swagger.converter.ModelConverter
import io.swagger.converter.ModelConverterContext
import io.swagger.converter.ModelConverters
import io.swagger.models.Model
import io.swagger.models.properties.Property
import io.vertx.core.json.jackson.DatabindCodec
import java.lang.reflect.Type
import java.math.BigDecimal

class JacksonSerializerModelConverter(private val mapper: ObjectMapper) : ModelConverter {

  override fun resolveProperty(
    type: Type,
    context: ModelConverterContext?,
    annotations: Array<out Annotation>?,
    chain: MutableIterator<ModelConverter>
  ): Property? {
    val handledType = findMappedType(type)
    return if (chain.hasNext()) {
      chain.next().resolveProperty(handledType, context, annotations, chain)
    } else {
      null
    }
  }

  override fun resolve(type: Type, context: ModelConverterContext?, chain: MutableIterator<ModelConverter>): Model? {
    val handledType = findMappedType(type)
    return if (chain.hasNext()) {
      chain.next().resolve(handledType, context, chain)
    } else {
      null
    }
  }

  private fun findMappedType(type: Type): Type {
    val javaType = mapper.constructType(type)
    val jsonSerializer = mapper.serializerProviderInstance.findTypedValueSerializer(javaType, true, null)
    return jsonSerializer.handledType() ?: type
  }
}

class BraidJacksonModule() : SimpleModule() {
  init {
    addSerializer(JsonRPCRequest::class.java, JsonRPCReqestSerializer())
    addSerializer(JsonRPCResultResponse::class.java, JsonRPCResultResponseSerializer())
    addSerializer(BigDecimal::class.java, ToStringSerializer.instance)
    addDeserializer(BigDecimal::class.java, NumberDeserializers.BigDecimalDeserializer())
  }
}

object BraidJacksonInit {
  init {
    listOf(
      KotlinModule(),
      JavaTimeModule(),
      BraidJacksonModule()
    ).forEach {
      DatabindCodec.mapper().registerModule(it)
      DatabindCodec.prettyMapper().registerModule(it)
    }
    DatabindCodec.mapper().configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
    DatabindCodec.prettyMapper().configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
    ModelConverters.getInstance().addConverter(JacksonSerializerModelConverter(DatabindCodec.mapper()))
  }

  fun init() {
    // automatically init during class load
  }
}
