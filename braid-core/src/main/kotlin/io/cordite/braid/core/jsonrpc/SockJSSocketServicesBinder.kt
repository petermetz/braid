/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.core.jsonrpc

import io.cordite.braid.core.async.end
import io.cordite.braid.core.http.write
import io.cordite.braid.core.logging.loggerFor
import io.cordite.braid.core.meta.ServiceDescriptor
import io.cordite.braid.core.meta.defaultServiceEndpoint
import io.cordite.braid.core.service.ServiceExecutor
import io.vertx.core.Vertx
import io.vertx.ext.auth.AuthProvider
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.sockjs.SockJSHandler

object SockJSSocketServicesBinder {
  private val log = loggerFor<SockJSSocketServicesBinder>()
  fun bind(
    vertx: Vertx,
    router: Router,
    port: Int,
    isSsl: Boolean,
    rootPath: String = "/api/",
    authProvider: AuthProvider?,
    threadPoolSize: Int = 1,
    serviceMap: Map<String, ServiceExecutor>
  ) {
    val sockJSHandler = SockJSHandler.create(vertx)
    val handler = SockJSSocketServicesHandler(
      vertx,
      rootPath,
      authProvider,
      threadPoolSize,
      serviceMap
    )
    sockJSHandler.socketHandler(handler)
    println("Mounting braid services...")
    log.info("Root API mount for braid: $rootPath")
    mountServices(
      serviceMap.keys,
      router,
      rootPath,
      port,
      isSsl,
      sockJSHandler
    )
    bindApiDocs(router, rootPath, serviceMap)
  }

  private fun bindApiDocs(
    router: Router,
    rootPath: String,
    serviceMap: Map<String, ServiceExecutor>
  ) {
    router.get(rootPath).handler {
      val services = serviceMap.keys
      it.write(ServiceDescriptor.createServiceDescriptors(rootPath, services))
    }
    val descriptors = serviceMap.map {
      it.key to it.value.getMethodDescriptors()
    }.toMap()

    router.get("${rootPath}:serviceName").handler {
      try {
        val serviceName = it.pathParam("serviceName")
        val serviceDoc = descriptors[serviceName]
        if (serviceDoc != null) {
          it.write(serviceDoc)
        } else {
          it.write(RuntimeException("could not find service $serviceName"))
        }
      } catch (err: Throwable) {
        log.error("failure in serving documentation for service", err)
        it.end(err);
      }
    }
  }

  private fun mountServices(
    serviceNames: Set<String>,
    router: Router,
    rootPath: String,
    port: Int,
    isSsl: Boolean,
    sockJSHandler: SockJSHandler
  ) {
    serviceNames.forEach {
      mountServiceName(
        it,
        router,
        rootPath,
        port,
        isSsl,
        sockJSHandler
      )
    }
  }

  private fun mountServiceName(
    serviceName: String,
    router: Router,
    rootPath: String,
    port: Int,
    isSsl: Boolean,
    sockJSHandler: SockJSHandler
  ) {
    val protocol = when (isSsl) {
      true -> "https"
      else -> "http"
    }
    val endpoint = defaultServiceEndpoint(rootPath, serviceName) + "/*"
    log.info("mounting braid service $serviceName to $protocol://localhost:${port}$endpoint")
    router.route(endpoint).handler(sockJSHandler)
  }
}