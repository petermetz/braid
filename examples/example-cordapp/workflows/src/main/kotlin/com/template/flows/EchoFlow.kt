/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.template.flows

import co.paralleluniverse.fibers.Suspendable
import net.corda.core.flows.*
import net.corda.core.identity.CordaX500Name
import net.corda.core.utilities.unwrap

@InitiatingFlow
@StartableByRPC
@StartableByService
class EchoFlow(private val text: String, private val counterPartyName: CordaX500Name) : FlowLogic<String>() {

  @Suspendable
  override fun call(): String {
    val counterParty = serviceHub.identityService.wellKnownPartyFromX500Name(counterPartyName)
      ?: error("failed to find $counterPartyName")
    val session = initiateFlow(counterParty)
    session.send(text)
    return session.receive<String>().unwrap { it }
  }
}

@InitiatedBy(EchoFlow::class)
class EchoResponder(private val session: FlowSession) : FlowLogic<Unit>() {

  @Suspendable
  override fun call() {
    val text = session.receive<String>().unwrap { it }
    session.send("Echo: $text")
  }
}