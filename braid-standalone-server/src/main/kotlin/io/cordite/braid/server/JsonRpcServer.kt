/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.server

import io.vertx.core.AsyncResult
import io.vertx.core.Future.failedFuture
import io.vertx.core.Future.succeededFuture
import io.vertx.core.Vertx

class JsonRpcServer private constructor(private val builder: JsonRpcServerBuilder) {
  companion object {
    init {
      io.cordite.braid.core.json.BraidJacksonInit.init()
    }

    internal fun createJsonRpcServer(builder: JsonRpcServerBuilder): JsonRpcServer {
      return JsonRpcServer(builder)
    }
  }

  var deploymentId: String? = null
  val rootPath get() = builder.rootPath

  init {
    if (builder.vertx == null) {
      builder.vertx = Vertx.vertx()
    }
  }

  /**
   * Start the server
   * The startup is asynchronous.
   */
  fun start() {
    start { }
  }

  /**
   * Start the server and callback on [callback] function
   * Clients should check the status of the [AsyncResult]
   */
  fun start(callback: (AsyncResult<Void>) -> Unit) {
    if (deploymentId == null) {
      with(builder) {
        vertx!!.deployVerticle(
          JsonRpcVerticle(
            rootPath,
            services,
            port,
            authProvider,
            httpServerOptions
          )
        ) {
          if (it.failed()) {
            println("failed to deploy: ${it.cause().message}")
            callback(failedFuture(it.cause()))
          } else {
            deploymentId = it.result()
            val protocol = if (builder.httpServerOptions.isSsl) "https" else "http"
            println("server mounted on $protocol://localhost:$port$rootPath")
            println("editor mounted on $protocol://localhost:$port")
            callback(succeededFuture())
          }
        }
      }
    }
  }

  /**
   * Asynchronously stop the sever
   */
  fun stop() {
    stop {}
  }

  /**
   * Asynchronously stop the server
   * Calls back on [callback] function with the result of the operation
   * Clients should check the [AsyncResult] state
   */
  fun stop(callback: (AsyncResult<Void>) -> Unit) {
    if (deploymentId != null) {
      builder.vertx!!.undeploy(deploymentId, callback)
      deploymentId = null
    }
  }
}

