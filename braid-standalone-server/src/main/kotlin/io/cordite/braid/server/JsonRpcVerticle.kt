/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.server

import io.cordite.braid.core.http.setupAllowAnyCORS
import io.cordite.braid.core.http.setupOptionsMethod
import io.cordite.braid.core.http.write
import io.cordite.braid.core.jsonrpc.SockJSSocketServiceBinder
import io.cordite.braid.core.logging.loggerFor
import io.cordite.braid.core.meta.ServiceDescriptor
import io.cordite.braid.core.meta.defaultServiceEndpoint
import io.cordite.braid.core.reflection.serviceName
import io.cordite.braid.core.service.ConcreteServiceExecutor
import io.cordite.braid.core.service.ServiceExecutor
import io.vertx.core.AbstractVerticle
import io.vertx.core.Future
import io.vertx.core.http.HttpServerOptions
import io.vertx.ext.auth.AuthProvider
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.StaticHandler
import io.vertx.ext.web.handler.sockjs.SockJSHandler
import io.vertx.ext.web.handler.sockjs.SockJSSocket

class JsonRpcVerticle(
  private val rootPath: String, val services: List<Any>, val port: Int,
  private val authProvider: AuthProvider?,
  private val httpServerOptions: HttpServerOptions
) : AbstractVerticle() {

  companion object {
    val logger = loggerFor<JsonRpcVerticle>()
  }

  private val serviceMap: Map<String, ServiceExecutor> by lazy {
    services.map { getServiceName(it) to wrapConcreteService(it) }.toMap()
  }

  private lateinit var router: Router
  private lateinit var sockJSHandler: SockJSHandler

  override fun start(startFuture: Future<Void>) {
    router = setupRouter()
    setupWebserver(router, startFuture)
  }

  private fun wrapConcreteService(service: Any): ServiceExecutor {
    return ConcreteServiceExecutor(service)
  }

  private fun ServiceExecutor.getConcreteExecutor(): ConcreteServiceExecutor? {
    return when (this) {
      is ConcreteServiceExecutor -> this
      else -> null
    }
  }

  private fun getJavaExecutorForService(serviceName: String): ConcreteServiceExecutor? {
    val service = serviceMap[serviceName] ?: return null
    return service.getConcreteExecutor()
  }

  private fun setupWebserver(router: Router, startFuture: Future<Void>) {
    vertx.createHttpServer(httpServerOptions.withCompatibleWebsockets())
      .requestHandler(router)
      .listen(port) {
        if (it.succeeded()) {
          logger.info("started on port $port")
          // the following is deprecated but there is no alternative for future passed in the start override
          @Suppress("DEPRECATION")
          startFuture.complete()
        } else {
          logger.error("failed to start because", it.cause())
          // the following is deprecated but there is no alternative for future passed in the start override
          @Suppress("DEPRECATION")
          startFuture.fail(it.cause())
        }
      }
  }

  private fun HttpServerOptions.withCompatibleWebsockets(): HttpServerOptions {
    webSocketSubProtocols = listOf("undefined")
    return this
  }

  private val servicesRouter: Router = Router.router(vertx)

  private fun setupRouter(): Router {
    val router = Router.router(vertx)
    router.setupAllowAnyCORS()
    router.setupOptionsMethod()
    setupSockJS()
    servicesRouter.post().handler(BodyHandler.create())
    servicesRouter.get("/").handler { it.getServiceList() }
    servicesRouter.get("/:serviceId").handler { it.getService(it.pathParam("serviceId")) }
    servicesRouter.get("/:serviceId/java").handler { it.getJavaImplementationHeaders(it.pathParam("serviceId")) }
    router.mountSubRouter(rootPath, servicesRouter)
    // the call to StaticHandler.create with classloader has been deprecated. An alternative is not clear.
    // have raised this question on the mailing list: https://groups.google.com/forum/#!topic/vertx/VyswZw23X4g
    @Suppress("DEPRECATION")
    router.get()
      .last()
      .handler(
        StaticHandler.create("editor-web", JsonRpcVerticle::class.java.classLoader)
          .setCachingEnabled(false)
          .setMaxCacheSize(1)
          .setCacheEntryTimeout(1)
      )
    return router
  }

  private fun RoutingContext.getServiceList() {
    val sm = ServiceDescriptor.createServiceDescriptors(rootPath, serviceMap.keys)
    write(sm)
  }

  private fun RoutingContext.getService(serviceName: String) {
    data class ServiceDocumentation(
      val java: String,
      val endpoint: String
    )

    val docs = ServiceDocumentation(
      "$rootPath$serviceName/java",
      defaultServiceEndpoint(rootPath, serviceName)
    )
    write(docs)
  }

  private fun RoutingContext.getJavaImplementationHeaders(serviceName: String) {
    val service = getJavaExecutorForService(serviceName)

    if (service == null) {
      write("")
      return
    } else {
      write(service.getMethodDescriptors())
    }
  }

  private fun socketHandler(socket: SockJSSocket) {
    val re = Regex("${rootPath.replace("/", "\\/")}([^\\/]+).*")
    val serviceName = re.matchEntire(socket.uri())?.groupValues?.get(1) ?: ""

    val service = serviceMap[serviceName]
    if (service != null) {
      SockJSSocketServiceBinder(vertx, service, authProvider).bind(socket)
    } else {
      socket.write("cannot find service $service")
      socket.close()
    }
  }

  private fun getServiceName(service: Any): String {
    return service.javaClass.serviceName()
  }

  private fun setupSockJS() {
    sockJSHandler = SockJSHandler.create(vertx)
    sockJSHandler.socketHandler(this::socketHandler)
    // mount each service

    servicesRouter.get("/:serviceId/braid/info").handler {
      val serviceId = it.pathParam("serviceId")
      if (serviceMap.contains(serviceId)) {
        it.next()
      } else {
        it.response()
          .setStatusMessage("""Braid: Service '$serviceId' does not exist. Click here to create it http://localhost:8080""".trimMargin())
          .setStatusCode(404)
          .end()
      }
    }
    serviceMap.keys.forEach {
      bindServiceSockJSHandler(it)
    }
  }

  private fun bindServiceSockJSHandler(serviceName: String) {
    servicesRouter.route("/$serviceName/braid/*").handler(sockJSHandler)
  }
}