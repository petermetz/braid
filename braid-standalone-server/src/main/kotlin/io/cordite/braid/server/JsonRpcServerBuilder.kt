/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.server

import io.cordite.braid.core.http.HttpServerConfig
import io.vertx.core.Vertx
import io.vertx.core.http.HttpServerOptions
import io.vertx.ext.auth.AuthProvider

/**
 * configuration options for JsonRPCServer
 */
class JsonRpcServerBuilder {

  internal var vertx: Vertx? = null
  internal var rootPath: String = "/api/"
  internal var port: Int = 8080
  internal var services: MutableList<Any> = mutableListOf()
  internal var authProvider: AuthProvider? = null
  internal var httpServerOptions: HttpServerOptions =
    HttpServerConfig.defaultServerOptions()

  companion object {
    /**
     * main entry point to setup a builder
     * following this, call the fluent api to setup options of the builder
     * and finally call [build]
     */
    @JvmStatic
    fun createServerBuilder() = JsonRpcServerBuilder()
  }

  /**
   * set the vertx container to run this server
   * default: creates a dedicated server
   */
  fun withVertx(vertx: Vertx): JsonRpcServerBuilder {
    this.vertx = vertx
    return this
  }

  /**
   * root http path that the services for this server are mounted
   * default is: /api/
   * note: path must end in /
   */
  fun withRootPath(rootPath: String): JsonRpcServerBuilder {
    if (!rootPath.endsWith('/')) {
      throw IllegalArgumentException("path must end with /")
    }
    this.rootPath = rootPath
    return this
  }

  /**
   * port that this server will bind to
   * default: 8080
   */
  fun withPort(port: Int): JsonRpcServerBuilder {
    this.port = port
    return this
  }

  /**
   * the set of services that will be exposed by this server
   * default: empty list
   */
  fun withServices(services: Collection<Any>): JsonRpcServerBuilder {
    this.services.clear()
    this.services.addAll(services)
    return this
  }

  /**
   * adds [service] to the list of services that will be exposed on this server
   */
  fun withService(service: Any): JsonRpcServerBuilder {
    this.services.add(service)
    return this
  }

  /**
   * sets the [AuthProvider] for authentication / authorisation when accessing a service
   * default: null - no auth and open access to all!
   */
  fun withAuthProvider(authProvider: AuthProvider?): JsonRpcServerBuilder {
    this.authProvider = authProvider
    return this
  }

  fun withHttpServerOptions(httpServerOptions: HttpServerOptions): JsonRpcServerBuilder {
    this.httpServerOptions = httpServerOptions
    return this
  }

  /**
   * build the server
   * don't forget to start the server using [JsonRpcServerBuilder.build]
   */
  fun build(): JsonRpcServer {
    return JsonRpcServer.createJsonRpcServer(this)
  }
}