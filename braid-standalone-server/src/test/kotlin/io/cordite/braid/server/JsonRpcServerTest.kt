/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.server

import io.cordite.braid.core.async.withPromise
import io.cordite.braid.core.http.failed
import io.cordite.braid.core.meta.defaultServiceEndpoint
import io.cordite.braid.core.meta.defaultServiceMountpoint
import io.cordite.braid.server.JsonRpcServerBuilder.Companion.createServerBuilder
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.VertxOptions
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpClientOptions
import io.vertx.core.json.JsonObject
import io.vertx.ext.unit.TestContext
import io.vertx.ext.unit.junit.VertxUnitRunner
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.net.ServerSocket

@RunWith(VertxUnitRunner::class)
class JsonRpcServerTest {

  private val port = getFreePort()
  private val vertx: Vertx =
    Vertx.vertx(VertxOptions().setBlockedThreadCheckInterval(30_000))
  private val server = createServerBuilder()
    .withVertx(vertx)
    .withPort(port)
    .withService(CalcService())
    .build()
  private val client = vertx.createHttpClient(
    HttpClientOptions()
      .setDefaultPort(port)
      .setDefaultHost("localhost")
      .setSsl(true)
      .setTrustAll(true)
      .setVerifyHost(false)
  )!!

  @Before
  fun before(context: TestContext) {
    server.start(context.asyncAssertSuccess<Void>()::handle)
  }

  @After
  fun after() {
    server.stop()
  }

  @Test
  fun `that we can list services and create them`(testContext: TestContext) {
    val serviceName = "calc"
    httpGetAsJsonObject("/api/")
      .onSuccess {
        testContext.assertEquals(1, it.size())
        testContext.assertTrue(it.containsKey(serviceName))
        val serviceDef = it.getJsonObject(serviceName)
        testContext.assertTrue(
          serviceDef.containsKey("endpoint") && serviceDef.containsKey(
            "documentation"
          )
        )
        testContext.assertEquals(
          defaultServiceEndpoint(serviceName),
          serviceDef.getString("endpoint")
        )
        testContext.assertEquals(
          defaultServiceMountpoint(serviceName),
          serviceDef.getString("documentation")
        )
      }
      .setHandler(testContext.asyncAssertSuccess())
  }

  private fun httpGetAsJsonObject(url: String): Future<JsonObject> {
    return httpGet(url).map { JsonObject(it) }
  }

  private fun httpGet(url: String): Future<Buffer> {
    return withPromise<Buffer> { promise ->
      try {
        @Suppress("DEPRECATION")
        client.get(url) { response ->
          if (response.failed) {
            promise.fail(response.statusMessage() ?: "failed")
          } else {
            response.bodyHandler {
              promise.complete(it)
            }
          }
        }.exceptionHandler {
          promise.fail(it)
        }.end()
      } catch (err: Throwable) {
        promise.fail(err)
      }
    }.future()
  }

  private fun getFreePort(): Int {
    return (ServerSocket(0)).use {
      it.localPort
    }
  }
}