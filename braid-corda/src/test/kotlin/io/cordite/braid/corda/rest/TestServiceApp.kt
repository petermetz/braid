/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.rest

import io.cordite.braid.corda.BraidConfig
import io.cordite.braid.corda.BraidServer
import io.cordite.braid.corda.utils.TestAuthProvider
import io.cordite.braid.core.http.HttpServerConfig
import io.cordite.braid.core.rest.AuthSchema
import io.cordite.braid.core.rest.RestConfig
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.ext.auth.AuthProvider
import io.vertx.ext.auth.jwt.JWTAuth
import java.io.File

class TestServiceApp(
  port: Int,
  private val username: String,
  private val password: String
) {

  private val service: TestService =
    TestServiceImpl()

  companion object {
    const val SWAGGER_ROOT = ""
    const val REST_API_ROOT = "/rest-api"
    const val TEST_JWT_SECRET = "kul@sh@k3r"

    @JvmStatic
    fun main(args: Array<String>) {
      TestServiceApp(8080, "admin", "admin")
    }
  }

  private val tempJKS = File.createTempFile("temp-", ".jceks")!!
  private val jwtSecret = "secret"
  private lateinit var jwtAuth: JWTAuth

  val server: BraidServer

  init {
    server = BraidConfig()
      .withPort(port)
      .withService("test-service", service)
      .withAuthConstructor(this::createAuthProvider)
      .withHttpServerOptions(
        HttpServerConfig.defaultServerOptions().setSsl(false)
      )
      .withRestConfig(
        RestConfig(serviceName = "my-service")
          .withAuthSchema(AuthSchema.Token)
          .withSwaggerPath(SWAGGER_ROOT)
          .withApiPath(REST_API_ROOT)
          .withDebugMode()
          .withJwtSecret(TEST_JWT_SECRET)
          .withPaths {
            group("General Ledger") {
              unprotected {
                get("/hello-async", service::sayHelloAsync)
                get("/quiet-async-void", service::quietAsyncVoid)
                get("/quiet-async-unit", service::quietAsyncUnit)
                get("/quiet-unit", service::quietUnit)
                get("/hello", service::sayHello)
                get("/buffer", service::getBuffer)
                get("/bytearray", service::getByteArray)
                get("/bytebuf", service::getByteBuf)
                get("/bytebuffer", service::getByteBuffer)
                post("/doublebuffer", service::doubleBuffer)
                post("/custom", service::somethingCustom)
                get("/stringlist", service::returnsListOfStuff)
                get("/willfail", service::willFail)
                get("/headers/list/string", service::headerListOfStrings)
                get("/headers/list/int", service::headerListOfInt)
                get("/headers", service::headers)
                get("/headers/optional", service::optionalHeader)
                get("/headers/non-optional", service::nonOptionalHeader)
                get("/party", service::getCordaX500Name)
              }
              protected {
                post("/echo", service::echo)
              }
            }
          })
      .bootstrapBraid(TestAppServiceHub)
  }

  fun whenReady(): Future<String> = server.whenReady()
  fun shutdown() = server.shutdown()

  private fun createAuthProvider(@Suppress("UNUSED_PARAMETER") vertx: Vertx): AuthProvider {
    return TestAuthProvider(username, password)
  }
}