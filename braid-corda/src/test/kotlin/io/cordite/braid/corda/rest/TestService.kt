/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
@file:Suppress("DEPRECATION")

package io.cordite.braid.corda.rest

import io.netty.buffer.ByteBuf
import io.netty.handler.codec.http.HttpHeaderValues
import io.swagger.annotations.*
import io.vertx.core.Future
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpHeaders
import io.vertx.core.json.Json
import io.vertx.ext.web.RoutingContext
import net.corda.core.identity.CordaX500Name
import java.nio.ByteBuffer
import javax.ws.rs.HeaderParam
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType

const val X_HEADER_LIST_STRING = "x-list-string"
const val X_HEADER_STRING = "x-string"

interface TestService {
  fun sayHello(): String
  fun sayHelloAsync(): Future<String>?
  fun quietAsyncVoid(): Future<Void>
  fun quietAsyncUnit(): Future<Unit>
  fun quietUnit(): Unit
  fun echo(msg: String): String
  fun getBuffer(): Buffer
  fun getByteArray(): ByteArray
  fun getByteBuf(): ByteBuf
  fun getByteBuffer(): ByteBuffer
  fun doubleBuffer(bytes: Buffer): Buffer

  @ApiOperation(
    value = "do something custom",
    response = String::class,
    consumes = MediaType.TEXT_PLAIN,
    produces = MediaType.TEXT_PLAIN
  )
  @ApiImplicitParams(
    ApiImplicitParam(
      name = "name",
      value = "name parameter",
      dataTypeClass = String::class,
      paramType = "path",
      defaultValue = "Margaret",
      required = true,
      examples = Example(
        ExampleProperty("Satoshi"),
        ExampleProperty("Margaret"),
        ExampleProperty("Alan")
      )
    )
  )
  fun somethingCustom(rc: RoutingContext)

  @ApiOperation(
    value = "return list of strings",
    response = String::class,
    responseContainer = "List"
  )
  fun returnsListOfStuff(context: RoutingContext)

  fun willFail(): String
  fun headerListOfStrings(@HeaderParam(X_HEADER_LIST_STRING) value: List<String>): List<String>
  fun headerListOfInt(@HeaderParam(X_HEADER_LIST_STRING) value: List<Int>): List<Int>
  fun optionalHeader(@HeaderParam(X_HEADER_STRING) value: String?): String
  fun nonOptionalHeader(@HeaderParam(X_HEADER_STRING) value: String): String
  fun headers(@Context headers: javax.ws.rs.core.HttpHeaders): List<Int>
  fun getCordaX500Name(): CordaX500Name
}

class TestServiceImpl : TestService {
  override fun sayHello() = "hello"
  override fun sayHelloAsync() = Future.succeededFuture<String>("hello, async!")
  override fun quietAsyncVoid(): Future<Void> = Future.succeededFuture()
  override fun quietAsyncUnit(): Future<Unit> = Future.succeededFuture()
  override fun quietUnit(): Unit = Unit
  override fun echo(msg: String) = "echo: $msg"
  override fun getBuffer(): Buffer = Buffer.buffer("hello")
  override fun getByteArray(): ByteArray = Buffer.buffer("hello").bytes
  override fun getByteBuf(): ByteBuf = Buffer.buffer("hello").byteBuf
  override fun getByteBuffer(): ByteBuffer = Buffer.buffer("hello").byteBuf.nioBuffer()
  override fun doubleBuffer(bytes: Buffer): Buffer =
    Buffer.buffer(bytes.length() * 2)
      .appendBytes(bytes.bytes)
      .appendBytes(bytes.bytes)

  @ApiOperation(
    value = "do something custom",
    response = String::class,
    consumes = MediaType.TEXT_PLAIN,
    produces = MediaType.TEXT_PLAIN
  )
  @ApiImplicitParams(
    ApiImplicitParam(
      name = "name",
      value = "name parameter",
      dataTypeClass = String::class,
      paramType = "path",
      defaultValue = "Margaret",
      required = true,
      examples = Example(
        ExampleProperty("Satoshi"),
        ExampleProperty("Margaret"),
        ExampleProperty("Alan")
      )
    )
  )
  override fun somethingCustom(rc: RoutingContext) {
    val name = rc.request().getParam("foo") ?: "Margaret"
    rc.response().putHeader(HttpHeaders.CONTENT_TYPE, HttpHeaderValues.APPLICATION_JSON)
      .setChunked(true).end(Json.encode("Hello, $name!"))
  }

  @ApiOperation(
    value = "return list of strings",
    response = String::class,
    responseContainer = "List"
  )
  override fun returnsListOfStuff(context: RoutingContext) {
    context.response()
      .putHeader(HttpHeaders.CONTENT_TYPE, HttpHeaderValues.APPLICATION_JSON)
      .setChunked(true).end(Json.encode(listOf("one", "two")))
  }

  override fun willFail(): String {
    throw RuntimeException("total fail!")
  }

  override fun headerListOfStrings(@HeaderParam(X_HEADER_LIST_STRING) value: List<String>): List<String> {
    return value
  }

  override fun headerListOfInt(@HeaderParam(X_HEADER_LIST_STRING) value: List<Int>): List<Int> {
    return value
  }

  override fun optionalHeader(@HeaderParam(X_HEADER_STRING) value: String?): String {
    return value ?: "null"
  }

  override fun nonOptionalHeader(@HeaderParam(X_HEADER_STRING) value: String): String {
    return value
  }

  override fun headers(@Context headers: javax.ws.rs.core.HttpHeaders): List<Int> {
    val acceptableLanguages = headers.acceptableLanguages
    assert(acceptableLanguages.size == 1 && acceptableLanguages.first().language == "*")
    val acceptableMediaTypes = headers.acceptableMediaTypes
    assert(acceptableMediaTypes.size == 1 && acceptableMediaTypes.first() == MediaType.WILDCARD_TYPE)
    val cookies = headers.cookies
    assert(cookies.isEmpty())
    val date = headers.date
    assert(date == null)
    val language = headers.language
    assert(language == null)
    val length = headers.length
    assert(length == -1)
    val mediaType = headers.mediaType
    assert(mediaType == null)

    return headers.getRequestHeader(X_HEADER_LIST_STRING).map { it.toInt() }
  }

  override fun getCordaX500Name(): CordaX500Name {
    return CordaX500Name("party", "London", "GB")
  }
}


