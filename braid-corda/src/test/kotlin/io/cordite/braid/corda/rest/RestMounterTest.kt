/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.rest

import io.cordite.braid.client.BraidClient
import io.cordite.braid.client.BraidClientConfig
import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.http.*
import io.cordite.braid.core.rest.LoginRequest
import io.cordite.braid.core.socket.findFreePort
import io.netty.handler.codec.http.HttpHeaderValues.APPLICATION_OCTET_STREAM
import io.vertx.core.Promise
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpClientOptions
import io.vertx.core.http.HttpHeaders.AUTHORIZATION
import io.vertx.core.http.HttpHeaders.CONTENT_TYPE
import io.vertx.core.json.JsonObject
import io.vertx.ext.auth.PubSecKeyOptions
import io.vertx.ext.auth.User
import io.vertx.ext.auth.jwt.JWTAuth
import io.vertx.ext.auth.jwt.JWTAuthOptions
import io.vertx.ext.unit.TestContext
import io.vertx.ext.unit.junit.VertxUnitRunner
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import net.corda.core.utilities.contextLogger
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import java.net.URI
import kotlin.test.assertEquals
import kotlin.test.assertTrue

@Suppress("DEPRECATION")
@RunWith(VertxUnitRunner::class)
class RestMounterTest {

  companion object {
    private val log = contextLogger()
    private const val username = "fuzz"
    private const val password = "ball"
    private val port = findFreePort()
    private val service = TestServiceApp(port, username, password)
    private val client = service.server.vertx.createHttpClient(
      HttpClientOptions()
        .setDefaultHost("localhost")
        .setDefaultPort(port)
    )
    private val headerValues = listOf(1, 2, 3)

    @JvmStatic
    @BeforeClass
    fun beforeClass(context: TestContext) {
      service.whenReady().setHandler(context.asyncAssertSuccess())
    }

    @JvmStatic
    @AfterClass
    fun afterClass() {
      service.shutdown()
      client.close()
    }
  }

  @Test
  fun `that we can retrieve the swagger json`() {
    @Suppress("DEPRECATION")
    val swaggerJson = client
      .futureGet(path = "${TestServiceApp.SWAGGER_ROOT}/swagger.json")
      .onSuccess { it.verifyNoError() }
      .compose { it.body() }
      .map { JsonObject(it) }
      .getOrThrow()
    println(swaggerJson)
  }

  @Test
  fun `that we can retrieve the swagger website`() {
    client.futureGet("${TestServiceApp.SWAGGER_ROOT}/")
      .onSuccess { it.verifyNoError() }
      .compose { it.bodyAsString() }
      .onSuccess { body -> assertTrue(body.contains("<title>Swagger UI</title>", true)) }
      .getOrThrow()
  }

  @Test
  fun `that we call the hello method`() {
    client.futureGet("${TestServiceApp.REST_API_ROOT}/hello")
      .onSuccess { it.verifyNoError() }
      .compose { it.bodyAsString() }
      .onSuccess { body -> assertEquals("hello", body) }
      .getOrThrow()
  }

  @Test
  fun `that we can retrieve a binary buffer`() {
    client.futureGet("${TestServiceApp.REST_API_ROOT}/buffer")
      .onSuccess { it.verifyNoError() }
      .compose { response ->
        assertEquals(APPLICATION_OCTET_STREAM.toString(), response.getHeader(CONTENT_TYPE))
        response.bodyAsString()
      }
      .onSuccess { body -> assertEquals("hello", body.toString()) }
      .getOrThrow()
  }

  @Test
  fun `that we can return a byte array`() {
    client.futureGet("${TestServiceApp.REST_API_ROOT}/bytearray")
      .onSuccess { it.verifyNoError() }
      .compose { response ->
        assertEquals(APPLICATION_OCTET_STREAM.toString(), response.getHeader(CONTENT_TYPE))
        response.bodyAsString()
      }
      .onSuccess { body -> assertEquals("hello", body.toString()) }
      .getOrThrow()
  }

  @Test
  fun `that we can return a bytebuf`() {
    client.futureGet("${TestServiceApp.REST_API_ROOT}/bytebuf")
      .onSuccess { it.verifyNoError() }
      .compose { response ->
        assertEquals(
          APPLICATION_OCTET_STREAM.toString(),
          response.getHeader(CONTENT_TYPE)
        )
        response.bodyAsString()
      }
      .onSuccess { body -> assertEquals("hello", body) }
      .getOrThrow()
  }

  @Test
  fun `that we can return a bytebuffer`() {
    client.futureGet("${TestServiceApp.REST_API_ROOT}/bytebuffer")
      .onSuccess { it.verifyNoError() }
      .compose { response ->
        assertEquals(
          APPLICATION_OCTET_STREAM.toString(),
          response.getHeader(CONTENT_TYPE)
        )
        response.bodyAsString()
      }
      .onSuccess { body -> assertEquals("hello", body) }
      .getOrThrow()
  }

  @Test
  fun `that we can send and receive a buffer`() {
    val bytes = Buffer.buffer("hello")
    client.futurePost(path = "${TestServiceApp.REST_API_ROOT}/doublebuffer", body = bytes)
      .onSuccess { it.verifyNoError() }
      .compose { response ->
        assertEquals(APPLICATION_OCTET_STREAM.toString(), response.getHeader(CONTENT_TYPE))
        response.bodyAsString()
      }
      .onSuccess { body ->
        assertEquals("hellohello", body.toString())
      }
      .getOrThrow()
  }

  @Test
  fun `that we can login and call a protected method`() {
    val accessToken = client
      .futurePost(
        path = "${TestServiceApp.REST_API_ROOT}/auth/login", body = LoginRequest(
          username,
          password
        )
      )
      .map { it.verifyNoError() }
      .compose { it.bodyAsString() }
      .getOrThrow()

    // verify that the JWT has been correctly encrypted with the jwt secret provided in RestConfig
    val jwtAuth = JWTAuth.create(
      service.server.vertx, JWTAuthOptions()
        .addPubSecKey(
          PubSecKeyOptions()
            .setAlgorithm("HS256")
            .setPublicKey(TestServiceApp.TEST_JWT_SECRET)
            .setSymmetric(true)
        )
    )

    val decodedUserFuture = Promise.promise<User>()
    jwtAuth.authenticate(json { obj("jwt" to accessToken) }, decodedUserFuture::handle)
    val decodedUser = decodedUserFuture.future().getOrThrow()

    log.info("decoded user is ${decodedUser.principal()}")
    assertEquals(username, decodedUser.principal().getString("user"))
    client.futurePost(
      path = "${TestServiceApp.REST_API_ROOT}/echo",
      body = "hello",
      headers = mapOf(AUTHORIZATION.toString() to "Bearer $accessToken")
    )
      .onSuccess { it.verifyNoError() }
      .compose { request -> request.bodyAsString() }
      .onSuccess { body ->
        assertEquals("echo: hello", body)
      }
      .getOrThrow()
  }

  @Test
  fun `that we can send a list of items as a header with a single name`() {
    client.futureGet(
      path = "${TestServiceApp.REST_API_ROOT}/headers/list/string",
      headers = mapOf(X_HEADER_LIST_STRING to headerValues.map { it.toString() })
    )
      .onSuccess { it.verifyNoError() }
      .compose { request -> request.bodyAsJsonList<String>() }
      .onSuccess { body -> assertEquals(headerValues.map { it.toString() }, body) }
      .getOrThrow()

  }

  @Test
  fun `that we can send a list as headers to a function that receives ints`() {
    client.futureGet(
      path = "${TestServiceApp.REST_API_ROOT}/headers/list/int",
      headers = mapOf(X_HEADER_LIST_STRING to headerValues)
    )
      .onSuccess { it.verifyNoError() }
      .compose { request -> request.bodyAsJsonList<String>() }
      .onSuccess { body ->
        assertEquals(headerValues.map { it.toString() }, body)
      }
      .getOrThrow()
  }

  @Test
  fun `that we can send a bunch of headers and return them in the body payload`() {
    client.futureGet(
      path = "${TestServiceApp.REST_API_ROOT}/headers",
      headers = mapOf(X_HEADER_LIST_STRING to headerValues)
    )
      .onSuccess { it.verifyNoError() }
      .compose { request -> request.bodyAsJsonList<String>() }
      .onSuccess { body ->
        assertEquals(headerValues.map { it.toString() }, body)
      }
      .getOrThrow()
  }

  @Test
  fun `that we can send headers to method that receives them optionally`() {
    val testString = "this is a test"
    client.futureGet(
      path = "${TestServiceApp.REST_API_ROOT}/headers/optional",
      headers = mapOf(X_HEADER_STRING to testString)
    )
      .onSuccess { it.verifyNoError() }
      .compose { request -> request.bodyAsString() }
      .onSuccess { body -> assertEquals(testString, body) }
      .getOrThrow()
  }

  @Test
  fun `that we can call a method that receives headers optionally with no headers`() {
    // N.B. no header set
    client.futureGet(path = "${TestServiceApp.REST_API_ROOT}/headers/optional")
      .onSuccess { it.verifyNoError() }
      .compose { request -> request.bodyAsString() }
      .onSuccess { body -> assertEquals("null", body) }
      .getOrThrow()
  }

  @Test
  fun `that calling a method that receives header non-optionally without headers causes an exception`() {
    // N.B. no header set
    client.futureGet(path = "${TestServiceApp.REST_API_ROOT}/headers/non-optional")
      .onSuccess { it.verifyError(500) }
      .compose { request -> request.bodyAsString() }
      .onSuccess { body -> assertEquals("method requires header $X_HEADER_STRING but it was missing", body) }
      .getOrThrow()
  }

  @Test
  fun `test that method that fails returns all headers`(context: TestContext) {
    val client = service.server.vertx.createHttpClient()
    val async1 = context.async()
    client.get(port, "localhost", "${TestServiceApp.REST_API_ROOT}/willfail")
      .exceptionHandler(context::fail)
      .handler {
        context.assertEquals(500, it.statusCode())
        context.assertEquals("total fail!", it.statusMessage())
        val expectedContentType = "text/plain; charset=utf8"
        context.assertEquals(expectedContentType, it.getHeader(CONTENT_TYPE))

        it.bodyHandler {
          val body = it.toString("utf8")
          context.assertEquals("total fail!", body)
          async1.complete()
        }
      }
      .end()
  }

  @Test
  fun `that we can invoke a protected json-rpc method`() {
    val client = BraidClient.createClient(
      BraidClientConfig(
        serviceURI = URI("http://localhost:$port/api/test-service/braid"),
        tls = false,
        authCredentials = LoginRequest(username, password)
      )
    )
    val proxy = client.bind(TestService::class.java)
    val message = "hello"
    val result = proxy.echo(message)
    assertEquals("echo: $message", result)
  }
}

