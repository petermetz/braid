/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.serialisation

import io.cordite.braid.corda.rest.PARTY_A_IDENTITY
import io.cordite.braid.corda.rest.TestAppServiceHub
import io.vertx.core.json.Json
import net.corda.core.contracts.Amount
import net.corda.core.contracts.Issued
import net.corda.core.contracts.PartyAndReference
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.AnonymousParty
import net.corda.core.identity.Party
import net.corda.core.utilities.OpaqueBytes
import net.corda.core.utilities.contextLogger
import net.corda.finance.GBP
import net.corda.testing.core.SerializationEnvironmentRule
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class SerialisationTests {
  companion object {
    private val log = contextLogger()
  }

  @Rule
  @JvmField
  val testSerialization = SerializationEnvironmentRule()

  @Before
  fun before() {
    BraidCordaJacksonInit.init(TestAppServiceHub)
  }

  @Test
  fun `that Amount of String token can be serialised and deserialised`() {
    val expected = Amount(100, "XOX")
    log.info("expected: $expected")
    val encoded = Json.encode(expected)
    log.info("encoded: $encoded")
    val actual = Json.decodeValue(encoded, Amount::class.java)
    log.info("actual: $actual")
    assertEquals(expected, actual)
  }

  @Test
  fun `that Amount of Currency token can be serialised and deserialised`() {
    val expected = Amount(100, GBP)
    log.info("expected: $expected")
    val encoded = Json.encode(expected)
    log.info("encoded: $encoded")
    val actual = Json.decodeValue(encoded, Amount::class.java)
    log.info("actual: $actual")
    assertEquals(expected, actual)
  }

  @Test
  fun `that we can de-serialize PartyAndReference`() {
    val expected = PartyAndReference(PARTY_A_IDENTITY.identity.party, OpaqueBytes.of(0x01))
    log.info("expected: $expected")
    val encoded = Json.encode(expected)
    log.info("encoded: $encoded")
    val actual = Json.decodeValue(encoded, PartyAndReference::class.java)
    log.info("actual: $actual")
    assertEquals(expected, actual)
  }

  @Test
  fun `that we can de-serialize Issued`() {
    val expected = Issued(PartyAndReference(PARTY_A_IDENTITY.identity.party, OpaqueBytes.of(0x01)), GBP)
    log.info("expected: $expected")
    val encoded = Json.encode(expected)
    log.info("encoded: $encoded")
    val actual = Json.decodeValue(encoded, Issued::class.java)
    log.info("actual: $actual")
    assertEquals(expected, actual)
  }

  @Test
  fun `that Amount of Issued Currency can be serialised and deserialised`() {
    val expected = Amount(100, Issued(PartyAndReference(PARTY_A_IDENTITY.identity.party, OpaqueBytes.of(0x01)), GBP))
    log.info("expected: $expected")
    val encoded = Json.encode(expected)
    log.info("encoded: $encoded")
    val actual = Json.decodeValue(encoded, Amount::class.java)
    log.info("actual: $actual")
    assertEquals(expected, actual)
  }

  @Test
  fun `that we can serialize and deserialize a party`() {
    val partyASerialized = Json.encode(PARTY_A_IDENTITY.party)
    val deserializedParty = Json.decodeValue(partyASerialized, Party::class.java)
    assertEquals(PARTY_A_IDENTITY.party, deserializedParty)
  }

  @Test
  fun `that we can serialize a Party and deserialize as an AbstractParty`() {
    val partyASerialized = Json.encode(PARTY_A_IDENTITY.party)
    val deserializedParty = Json.decodeValue(partyASerialized, AbstractParty::class.java)
    assertEquals(PARTY_A_IDENTITY.party, deserializedParty)
  }

  @Test
  fun `that we can serialize and deserialize an anonymous party`() {
    val anonymousIdentity = TestAppServiceHub.keyManagementService.freshKeyAndCert(PARTY_A_IDENTITY.identity, false)
    val anonymousParty = AnonymousParty(anonymousIdentity.owningKey)
    val partyASerialized = Json.encode(anonymousParty)
    val deserializedParty = Json.decodeValue(partyASerialized, AnonymousParty::class.java)
    assertEquals(anonymousParty, deserializedParty)
  }

  @Test
  fun `that we can serialize anonymous party and deserialize as an abstract party`() {
    val anonymousIdentity = TestAppServiceHub.keyManagementService.freshKeyAndCert(PARTY_A_IDENTITY.identity, false)
    val anonymousParty = AnonymousParty(anonymousIdentity.owningKey)
    val partyASerialized = Json.encode(anonymousParty)
    val deserializedParty = Json.decodeValue(partyASerialized, AbstractParty::class.java)
    assertTrue(deserializedParty is AnonymousParty)
    assertEquals(anonymousParty, deserializedParty)
  }
}
