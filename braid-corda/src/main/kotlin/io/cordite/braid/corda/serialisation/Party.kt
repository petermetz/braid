/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.serialisation

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.AnonymousParty
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.node.AppServiceHub
import net.corda.core.node.services.IdentityService
import java.security.PublicKey

object PartySerializer : StdSerializer<Party>(Party::class.java) {

  override fun serialize(value: Party?, generator: JsonGenerator, provider: SerializerProvider?) {
    generator.writeString(value?.name?.toString() ?: "null")
  }
}

object AnonymousPartySerializer : StdSerializer<AnonymousParty>(AnonymousParty::class.java) {

  override fun serialize(value: AnonymousParty?, generator: JsonGenerator, provider: SerializerProvider?) {
    generator.codec.writeValue(generator, value?.owningKey)
  }
}

object AbstractPartyDeserializer : StdDeserializer<AbstractParty>(AbstractParty::class.java) {

  override fun deserialize(parser: JsonParser, ctxt: DeserializationContext): AbstractParty {
    return try {
      // attempt to parser as a well-known party
      parser.codec.readValue<Party>(parser, Party::class.java)
    } catch (err: Throwable) {
      parser.codec.readValue<AnonymousParty>(parser, AnonymousParty::class.java)
    }
  }
}

class PartyDeserializer(private val identityService: IdentityService?) : StdDeserializer<Party>(Party::class.java) {

  override fun deserialize(parser: JsonParser, ctxt: DeserializationContext): Party {
    if (identityService == null) error("identityService has not been initialised for jackson. Did you call ${BraidCordaJacksonInit::class.simpleName} without a ${AppServiceHub::class.simpleName}?")

    val name = parser.codec.readValue(parser, CordaX500Name::class.java)
    return identityService.wellKnownPartyFromX500Name(name) ?: error("failed to find party from x500 name $name")
  }
}

object AnonymousPartyDeserializer : StdDeserializer<AnonymousParty>(AnonymousParty::class.java) {

  override fun deserialize(parser: JsonParser, ctxt: DeserializationContext): AnonymousParty {
    val publicKey = parser.codec.readValue(parser, PublicKey::class.java)
    return AnonymousParty(publicKey)
  }
}