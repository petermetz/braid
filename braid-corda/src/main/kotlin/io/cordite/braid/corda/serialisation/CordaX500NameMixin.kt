/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.serialisation

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import com.fasterxml.jackson.databind.util.StdConverter
import net.corda.core.identity.CordaX500Name

@Suppress("unused")
@JsonSerialize(converter = CordaX500ToStringConverter::class)
@JsonDeserialize(converter = StringToCordaX500Converter::class)
abstract class CordaX500NameMixin {
}

class CordaX500ToStringConverter : StdConverter<CordaX500Name, String>() {
  override fun convert(value: CordaX500Name): String {
    return value.toString()
  }
}

class StringToCordaX500Converter : StdConverter<String, CordaX500Name>() {
  override fun convert(value: String): CordaX500Name {
    return CordaX500Name.parse(value)
  }
}

object CordaX500NameSerializer : StdSerializer<CordaX500Name>(CordaX500Name::class.java) {
  override fun serialize(
    value: CordaX500Name,
    generator: JsonGenerator,
    provider: SerializerProvider
  ) {
    generator.writeString(value.toString())
  }
}

object CordaX500NameDeserializer :
  StdDeserializer<CordaX500Name>(CordaX500Name::class.java) {

  override fun deserialize(
    parser: JsonParser,
    context: DeserializationContext
  ): CordaX500Name {
    return CordaX500Name.parse(parser.text)
  }
}