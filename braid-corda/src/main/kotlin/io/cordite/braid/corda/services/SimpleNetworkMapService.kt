/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.services

import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.node.NodeInfo
import net.corda.core.node.services.NetworkMapCache
import net.corda.core.utilities.NetworkHostAndPort
import rx.Observable

interface SimpleNetworkMapService {
  data class SimpleNodeInfo(
    val addresses: List<NetworkHostAndPort>,
    val legalIdentities: List<Party>
  ) {

    // we map to work around the serialisation of
    constructor(nodeInfo: NodeInfo) : this(nodeInfo.addresses, nodeInfo.legalIdentities)
  }

  enum class MapChangeType {
    ADDED,
    REMOVED,
    MODIFIED,
    SNAPSHOT
  }

  data class MapChange(
    val type: MapChangeType,
    val node: SimpleNodeInfo,
    val previousNode: SimpleNodeInfo? = null
  ) {

    constructor(change: NetworkMapCache.MapChange) : this(
      when (change) {
        is NetworkMapCache.MapChange.Added -> MapChangeType.ADDED
        is NetworkMapCache.MapChange.Removed -> MapChangeType.REMOVED
        is NetworkMapCache.MapChange.Modified -> MapChangeType.MODIFIED
        else -> throw RuntimeException("unknown map change type ${change.javaClass}")
      },
      change.node.asSimple(),
      when (change) {
        is NetworkMapCache.MapChange.Modified -> change.previousNode.asSimple()
        else -> null
      }
    )
  }

  fun myNodeInfo(): SimpleNodeInfo
  fun allNodes(): List<SimpleNodeInfo>
  fun state(): Observable<List<MapChange>>
  fun notaryIdentities(): List<Party>
  fun getNotary(cordaX500Name: CordaX500Name): Party?
  fun getNodeByAddress(hostAndPort: String): SimpleNodeInfo?
  fun getNodeByLegalName(name: CordaX500Name): SimpleNodeInfo?
}

internal fun NetworkMapCache.MapChange.asSimple(): SimpleNetworkMapService.MapChange {
  return SimpleNetworkMapService.MapChange(this)
}

internal fun NodeInfo.asSimple(): SimpleNetworkMapService.SimpleNodeInfo {
  return SimpleNetworkMapService.SimpleNodeInfo(this)
}

