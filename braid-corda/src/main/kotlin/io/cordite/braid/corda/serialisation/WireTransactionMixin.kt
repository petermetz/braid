/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.serialisation

import com.fasterxml.jackson.annotation.JsonIgnore
import net.corda.core.contracts.ContractState
import net.corda.core.crypto.MerkleTree
import net.corda.core.crypto.SecureHash

@Suppress("unused")
abstract class WireTransactionMixin {

  @JsonIgnore
  abstract fun getMerkleTree(): MerkleTree

  @JsonIgnore
  abstract fun getAvailableComponents(): List<Any>

  @JsonIgnore
  abstract fun getAvailableComponentHashes(): List<SecureHash>

  @JsonIgnore
  abstract fun getOutputStates(): List<ContractState>
}