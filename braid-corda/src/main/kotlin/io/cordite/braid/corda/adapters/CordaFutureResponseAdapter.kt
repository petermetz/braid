/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.adapters

import io.cordite.braid.core.jsonrpc.JsonRPCRequest
import io.cordite.braid.core.service.JsonRpcResultAdapter
import net.corda.core.concurrent.CordaFuture
import net.corda.core.utilities.getOrThrow
import rx.Subscriber

object CordaFutureResponseAdapter : JsonRpcResultAdapter<CordaFuture<*>>() {

  override val handledClass: Class<CordaFuture<*>> = CordaFuture::class.java

  override fun handle(request: JsonRPCRequest, result: CordaFuture<*>?, subscriber: Subscriber<Any>) {
    result ?: error("result cannot be null")
    result.then {
      try {
        val value = it.getOrThrow()
        super.send(value, subscriber)
      } catch (err: Throwable) {
        super.send(err, subscriber)
      }
    }
  }
}