/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.serialisation

import com.fasterxml.jackson.databind.module.SimpleModule
import io.cordite.braid.core.BraidInit
import io.vertx.core.json.jackson.DatabindCodec
import net.corda.core.contracts.Amount
import net.corda.core.contracts.Issued
import net.corda.core.crypto.SecureHash
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.AnonymousParty
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.node.AppServiceHub
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.WireTransaction
import net.corda.core.utilities.OpaqueBytes
import java.security.PublicKey

class BraidCordaJacksonModule(private val appServiceHub: AppServiceHub) : SimpleModule() {
  init {
    addDeserializer(AbstractParty::class.java, AbstractPartyDeserializer)
    addSerializer(AnonymousParty::class.java, AnonymousPartySerializer)
    addDeserializer(AnonymousParty::class.java, AnonymousPartyDeserializer)
    addSerializer(Party::class.java, PartySerializer)
    addDeserializer(Party::class.java, PartyDeserializer(appServiceHub?.identityService))
    addSerializer(SecureHash::class.java, SecureHashSerializer)
    addSerializer(SecureHash.SHA256::class.java, SecureHashSerializer)
    addDeserializer(SecureHash::class.java, SecureHashDeserializer())
    addDeserializer(SecureHash.SHA256::class.java, SecureHashDeserializer())

    // For ed25519 pubkeys
    // TODO: Fix these
//          .addSerializer(EdDSAPublicKey::class.java, JacksonSupport.PublicKeySerializer)
//          .addDeserializer(EdDSAPublicKey::class.java, JacksonSupport.PublicKeyDeserializer)

    // For NodeInfo
    // TODO this tunnels the Kryo representation as a Base58 encoded string. Replace when RPC supports this.
//      .addSerializer(NodeInfo::class.java, NodeInfoSerializer)
//      .addDeserializer(NodeInfo::class.java, NodeInfoDeserializer)

    // For Amount
    // we do not use the Corda amount serialisers
//          .addSerializer(Amount::class.java, JacksonSupport.AmountSerializer)
//          .addDeserializer(Amount::class.java, JacksonSupport.AmountDeserializer)

    // For OpaqueBytes
    addDeserializer(OpaqueBytes::class.java, OpaqueBytesDeserializer())
    addSerializer(OpaqueBytes::class.java, OpaqueBytesSerializer())

    // For X.500 distinguished names
    addDeserializer(
      CordaX500Name::class.java,
      CordaX500NameDeserializer
    )
    addSerializer(CordaX500Name::class.java, CordaX500NameSerializer)

    // Mixins for transaction types to prevent some properties from being serialized
    setMixInAnnotation(
      SignedTransaction::class.java,
      SignedTransactionMixin::class.java
    )
    setMixInAnnotation(
      WireTransaction::class.java,
      WireTransactionMixin::class.java
    )
    setMixInAnnotation(
      CordaX500Name::class.java,
      CordaX500NameMixin::class.java
    )
    setMixInAnnotation(CordaX500Name::class.java, CordaX500NameMixin::class.java)
    addSerializer(PublicKey::class.java, PublicKeySerializer())
    addDeserializer(PublicKey::class.java, PublicKeyDeserializer())
    addSerializer(Amount::class.java, AmountSerializer())
    addDeserializer(Amount::class.java, AmountDeserializer())
    addSerializer(Issued::class.java, IssuedSerializer())
    addDeserializer(Issued::class.java, IssuedDeserializer())
  }

  override fun getModuleName(): String {
    return "braid-corda"
  }
}

object BraidCordaJacksonInit {

  fun init(appServiceHub: AppServiceHub) {
    BraidInit.init()
    val braidCordaModule = BraidCordaJacksonModule(appServiceHub)
    listOf(DatabindCodec.mapper(), DatabindCodec.prettyMapper()).forEach { mapper ->
      mapper.registerModule(braidCordaModule)
    }
    val ids = DatabindCodec.mapper().registeredModuleIds
  }
}

