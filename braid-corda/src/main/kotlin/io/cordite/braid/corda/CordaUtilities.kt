/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda

import io.vertx.core.Future
import io.vertx.core.Promise
import net.corda.core.concurrent.CordaFuture
import net.corda.core.internal.concurrent.doOnComplete
import net.corda.core.internal.concurrent.doOnError
import net.corda.core.node.AppServiceHub

object CordaUtilities {
  fun <V> CordaFuture<V>.toVertxFuture(): Future<V> {
    val promise = Promise.promise<V>()
    this.doOnComplete(promise::complete)
    this.doOnError(promise::fail)
    return promise.future()
  }

  fun <T> AppServiceHub.transaction(fn: () -> T): T {
    return database.transaction {
      fn()
    }
  }
}
