/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.serialisation

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import net.corda.core.contracts.StateRef
import net.corda.core.crypto.SecureHash
import net.corda.core.crypto.TransactionSignature
import net.corda.core.identity.Party
import net.corda.core.serialization.SerializedBytes
import net.corda.core.transactions.CoreTransaction
import net.corda.core.transactions.NotaryChangeWireTransaction
import net.corda.core.transactions.WireTransaction
import java.security.PublicKey

@Suppress("unused")
abstract class SignedTransactionMixin {

  @JsonIgnore
  abstract fun getTxBits(): SerializedBytes<CoreTransaction>
  @JsonProperty("signatures")
  protected abstract fun getSigs(): List<TransactionSignature>

  @JsonProperty
  protected abstract fun getTransaction(): CoreTransaction

  @JsonIgnore
  abstract fun getTx(): WireTransaction

  @JsonIgnore
  abstract fun getNotaryChangeTx(): NotaryChangeWireTransaction

  @JsonIgnore
  abstract fun getInputs(): List<StateRef>

  @JsonIgnore
  abstract fun getNotary(): Party?

  @JsonIgnore
  abstract fun getId(): SecureHash

  @JsonIgnore
  abstract fun getRequiredSigningKeys(): Set<PublicKey>
}