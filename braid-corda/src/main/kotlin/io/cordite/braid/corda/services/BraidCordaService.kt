/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.services

import io.cordite.braid.corda.BraidConfig
import io.cordite.braid.corda.services.BraidCordaService.Companion.startAllBraidServicesAndBindConfig
import io.github.classgraph.ClassGraph
import io.vertx.core.Vertx
import net.corda.core.node.AppServiceHub

/**
 * Use this class to make a class feel like a CordaService (but note that it must not use the [net.corda.core.node.services.CordaService] annotation)
 * and to be automatically constructable. Each implementor is passed a [BraidConfig] that it can use to bind itself
 * to Braid's transports.
 * The static method [startAllBraidServicesAndBindConfig] will scan for, construct, and initialise all [BraidCordaService].
 * NB your class *must* have a single argument constructor that takes an AppServiceHub
 */
interface BraidCordaService {

  companion object {

    /**
     * Start all classes that implement [BraidCordaService] and bind them to the [BraidConfig]
     * As a side-effect, this bootstraps the ancillary services such as [SimpleNetworkMapService]
     */
    fun startAllBraidServicesAndBindConfig(
      braidConfig: BraidConfig,
      serviceHub: AppServiceHub,
      vertx: Vertx? = null
    ): BraidConfig {
      val braidServices =
        createAllBraidServices(serviceHub)
      val preparedConfig =
        attachVertx(vertx, braidConfig)
//      val authBoundConfig = AuthRestBinding().configureWith(daslConfig, preparedConfig)
//      return braidServices.fold(authBoundConfig) { acc, service ->
//        service.configureWith(acc)
//      }
      return braidServices.fold(preparedConfig) { acc, service ->
        service.configureWith(acc)
      }
    }

    private fun attachVertx(vertx: Vertx?, braidConfig: BraidConfig): BraidConfig {
      return when (vertx) {
        null -> braidConfig.withVertx(Vertx.vertx())
        else -> braidConfig.withVertx(vertx)
      }
    }

    private fun createAllBraidServices(serviceHub: AppServiceHub): List<BraidCordaService> {
      val classes =
        findAllBraidServiceClasses()
      return classes.map {
        it.getConstructor(AppServiceHub::class.java).newInstance(serviceHub) as BraidCordaService
      }
    }

    private fun findAllBraidServiceClasses(): List<Class<out Any>> {
      val res = ClassGraph().enableClassInfo().scan()
      return res.getClassesImplementing(BraidCordaService::class.qualifiedName)
        .names.distinct().map { Class.forName(it) }
    }
  }

  /**
   * Used to customise the braid config - usually you'll just register yourself as a service
   */
  fun configureWith(config: BraidConfig): BraidConfig
}

