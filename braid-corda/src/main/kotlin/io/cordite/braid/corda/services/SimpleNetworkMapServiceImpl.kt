/**
 * Copyright 2018 Cordite Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.cordite.braid.corda.services

import io.cordite.braid.corda.BraidConfig
import io.cordite.braid.corda.CordaUtilities.transaction
import io.cordite.braid.corda.services.SimpleNetworkMapService.MapChange
import io.cordite.braid.corda.services.SimpleNetworkMapService.SimpleNodeInfo
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.node.AppServiceHub
import net.corda.core.utilities.NetworkHostAndPort
import rx.Observable
import rx.Subscription

class SimpleNetworkMapServiceImpl(
  private val services: AppServiceHub,
  private val config: BraidConfig
) : SimpleNetworkMapService {


  override fun myNodeInfo(): SimpleNodeInfo {
    return services.myInfo.asSimple()
  }

  override fun allNodes(): List<SimpleNodeInfo> {
    return services.networkMapCache.allNodes.map {
      it.asSimple()
    }
  }

  override fun state(): Observable<List<MapChange>> {

    return Observable.unsafeCreate { subscriber ->
      val dataFeed = services.networkMapCache.track()
      services.transaction {
        val snapshot =
          dataFeed.snapshot.map { MapChange(SimpleNetworkMapService.MapChangeType.SNAPSHOT, SimpleNodeInfo(it)) }
        subscriber.onNext(snapshot)
        var subscription: Subscription? = null

        subscription = dataFeed.updates.subscribe { change ->
          if (subscriber.isUnsubscribed) {
            subscription?.unsubscribe()
            subscription = null
          } else {
            subscriber.onNext(listOf(change.asSimple()))
          }
        }
      }
    }
  }

  override fun notaryIdentities(): List<Party> {
    return services.transaction {
      services.networkMapCache.notaryIdentities
    }
  }

  override fun getNotary(cordaX500Name: CordaX500Name): Party? {
    return services.transaction {
      services.networkMapCache.getNotary(cordaX500Name)
    }
  }

  override fun getNodeByAddress(hostAndPort: String): SimpleNodeInfo? {
    return services.transaction {
      services.networkMapCache.getNodeByAddress(NetworkHostAndPort.parse(hostAndPort))
        ?.asSimple()
    }
  }

  override fun getNodeByLegalName(name: CordaX500Name): SimpleNodeInfo? {
    return services.transaction {
      services.networkMapCache.getNodeByLegalName(name)?.asSimple()
    }
  }
}

